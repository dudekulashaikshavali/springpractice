package com.hcl.SpringBootHellodemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootHellodemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootHellodemoApplication.class, args);
	}

}
