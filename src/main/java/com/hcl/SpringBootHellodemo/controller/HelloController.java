package com.hcl.SpringBootHellodemo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/getid")
public class HelloController {
	
//	getid/hello
	@GetMapping("/hello")
	public String hello() {
		return "hello world";
		
	}
	
//getid/getmapping/25
	@GetMapping("/getmapping/{id}")
	@ResponseBody
	public String getparam(@PathVariable int id) {
		return "ID:" +id  ;
	}
	
//	getid/getmapping/api/21/shaiksha/software/22000/24
	
	@GetMapping("/getmapping/api/{id}/{name}/{jobrole}/{salary}/{age}")
	@ResponseBody
	public String getquery(@PathVariable int id,
			@PathVariable String name, @PathVariable String jobrole,
			@PathVariable double salary, @PathVariable int age) {
		return "ID:" +id + "Name:" + name + " JobRole: " + jobrole + " Salary: " + salary + "Age: " + age ;
	}
	
	
//	getid/request/param/pa?id=21&name=sai&jobrole=software&salary=35000&age=35
	@GetMapping("/request/param/pa") 
	@ResponseBody
	public String getRequestparam(@RequestParam int id,
									@RequestParam String name, 
									@RequestParam String jobrole,
									@RequestParam double salary,
									@RequestParam int age) {
		return "ID:" +id + "Name:" + name + " JobRole: " + jobrole + " Salary: " + salary + "Age: " + age ;
	}
	
//	getid/request/param/value
	 
	@GetMapping("/request/param/value") 
	@ResponseBody
	public String getRequestparamvalue(@RequestParam(defaultValue="21")int id,
			@RequestParam(defaultValue="Siva") String name, 
			@RequestParam(defaultValue="Tester") String jobrole,
			@RequestParam(defaultValue="25000") double salary,
			@RequestParam(defaultValue="25") int age) {
		return "ID:" +id + "Name:" + name + " JobRole: " + jobrole + " Salary: " + salary + "Age: " + age ;
	}
	
//	getid/listparam/value?id=21&name=shaiksha
	
@GetMapping("/listparam/value")
@ResponseBody
public String getParam(@RequestParam List<Integer>id,
		@RequestParam List<String>name) {
	return "this is my id:"+ id  + "my name is" + name;
	
}


@PostMapping("/poto")
@ResponseBody
public String getpostoperate(@RequestParam Integer id,
				@RequestParam String name){
					return "my id is:"+ id + "my name is:" + name;
				}


@PostMapping("/getpost/ui")
public String postmappingdata(@RequestParam(defaultValue="1") int id,
		@RequestParam(defaultValue="sainath") String name, 
		@RequestParam(defaultValue="architect") String jobrole,
		@RequestParam(defaultValue="27") int age) {
	return "id: "+ id + "name: " + name + "jobrole: " + jobrole + "age: " + age ; 
}

}
